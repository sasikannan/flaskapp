from app import app
from app.api.routes import site



app.register_blueprint(site)


if __name__ == '__main__':
    app.run()

