import unittest
from main import app
from flask import json

class FlaskAppTest(unittest.TestCase):

    def test_get(self):
        tester = app.test_client(self)
        response = tester.get("/")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_post(self):
        tester = app.test_client(self)
        info = {"name": "vijay", "email":"vijay@gmail.com","cgpa":8,"arrears":0,"semester":"sixth"}
        response = tester.post("/students", data=json.dumps(info), headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status_code, 200)

    def test_get_id(self):
        tester = app.test_client(self)
        response = tester.get("/students/1")
        self.assertEqual(response.status_code, 200)

    def test_get_all(self):
        tester = app.test_client(self)
        response = tester.get("/students/1")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        tester = app.test_client(self)
        info={"name":"vignesh", "email":"vignesh1995@gmail.com","cgpa":8,"arrears":0,"semester":"sixth"}
        response = tester.put("/students/1", data=json.dumps(info),headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        tester = app.test_client(self)
        response = tester.delete("/students/5")
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()




#python -m unittest test.FlaskAppTest.test_post
#python -m unittest test.FlaskAppTest.test_get_all
#python -m unittest test.FlaskAppTest.test_get_id
#python -m unittest test.FlaskAppTest.test_put
#python -m unittest testing.test.FlaskAppTest.test_delete
# coverage run testing/test.py
#coverage run test.py
#coverage report test.py