from flask import Blueprint,jsonify,request
from app.model.table import Student,db
from app.services.logics import Logics

site = Blueprint (__name__,"site")

@site.route("/",methods=['GET'])
def index():
    return ("<h1> WELCOME TO STUDENTS PROFILE <h1>")

@site.route("/students",methods=['POST'])
def post():
    name= request.json["name"]
    email = request.json["email"]
    cgpa= request.json["cgpa"]
    arrears=request.json["arrears"]
    semester=request.json["semester"]
    new_student = Student(name, email, cgpa, arrears, semester)
    if new_student:
        Logics.save_to_db_student(new_student)
        return Logics.stu_dict(new_student)
    return jsonify({'message':'students already exits or error occured'}),404

@site.route("/students",methods=["GET"])
def get():
    student= Logics.get_students()
    if student:
        stu = {"students": []}
        for i in student:
            stu_dict = Logics.stu_dict(i)
            stu["students"].append(stu_dict)
        return stu
    return jsonify({'message':'no student data create one'}),204


@site.route("/students/<id>",methods=["GET"])
def get_by_id(id):
    student= Logics.get_student(id)
    if student:
        return Logics.stu_dict(student)
    return jsonify({'message':'not found'}),204


@site.route("/students/<id>",methods=['PUT'])
def update_student(id):
    student= Logics.get_student(id)
    if student:
        name = request.json["name"]
        email = request.json["email"]
        cgpa = request.json["cgpa"]
        arrears = request.json["arrears"]
        semester = request.json["semester"]
        student.name = name
        student.email = email
        student.cgpa = cgpa
        student.arrears = arrears
        student.semester = semester
        Logics.update()
        # db.session.commit()
        return Logics.stu_dict(student)


    return jsonify({"message":"requested id not found"}),404

@site.route("/students/<id>",methods=['DELETE'])
def delete_student(id):
    student= Logics.get_student(id)
    Logics.delete_from_db_student(student)
    if student:
        return (f"{student} deleted")
    return jsonify({"message":"requested id not found"}),404





