from app import db
from app.model.table import Student


class Logics():

    def stu_dict(self):
        stu_dict = self.__dict__
        stu_dict.pop('_sa_instance_state')
        return stu_dict

    def get_all(self):
        stu_dict = self.__dict__
        stu_dict.pop('_sa_instance_state')

    def get_student(id):
         Students = Student.query.get(id)
         return Students

    def get_students():
        Students = Student.query.all()
        return Students

    def update():
        db.session.commit()

    def save_to_db_student(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db_student(self):
        db.session.delete(self)
        db.session.commit()










