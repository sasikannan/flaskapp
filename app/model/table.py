from app import db

class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    email = db.Column(db.String(80), unique=True)
    cgpa= db.Column(db.Integer)
    arrears = db.Column(db.Float)
    semester=db.Column(db.String(20))


    def __init__(self, name, email,cgpa,arrears,semester):
        self.name = name
        self.email = email
        self.cgpa = cgpa
        self.arrears = arrears
        self.semester= semester



db.create_all()